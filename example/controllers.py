#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *

from rest_framework.exceptions import PermissionDenied

class Examples(viewsets.ModelViewSet):
    serializer_class = ExampleSerializer
    queryset = Example.objects.all()

    '''def list(self, request, *args, **kwargs):
        serializer = ExampleSerializer(self.get_queryset(), many=True)
        print (request.user.auth_token)
        return Response({'result':serializer.data})'''

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
        	response = {}
        	response['a'] = serializer.data['a']
        	response['b'] = serializer.data['b']
        	response['hasilPenjumlahan'] = response['a'] + response['b']
        	examples = Example.objects.create(a=serializer.data['a'], b = serializer.data['b'], hasilPenjumlahan=serializer.data['a']+serializer.data['b'])
        	examples.save()

        	return Response(response, status=status.HTTP_201_CREATED)
