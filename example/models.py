from django.db import models
from django.contrib.auth.models import *

class Example(models.Model):
    contohId = models.AutoField(primary_key=True)
    a = models.IntegerField(default =0)
    b = models.IntegerField(default = 0)
    hasilPenjumlahan = models.IntegerField(default=0 )
    createdDate = models.DateTimeField(editable=False)
    lastModifiedDate = models.DateTimeField(null=True)
    def save(self, *args, **kwargs):
        if not self.contohId:
            self.createdDate = timezone.now()
        else:
            self.lastModifiedDate = timezone.now()
        return super(Example, self).save(*args, **kwargs)

    class Meta:
        db_table = 'example'
